#include "dungeon.h"

typedef struct playerdata{
	//X and Y are position of top left of the room.
	int x; 
	int y;
} player;

void createTunnelMap(player p, int hardmap[MAX_H][MAX_V],int Tmap[MAX_H][MAX_V]);

void createNoTunnelMap(player p, int hardmap[MAX_H][MAX_V],int Tmap[MAX_H][MAX_V]);