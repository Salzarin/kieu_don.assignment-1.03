#include "dungeon.h"

void createTunnelMap(player p, int hardmap[MAX_H][MAX_V],int Tmap[MAX_H][MAX_V]){
    int end = p.x+p.y*MAX_H;
    int i,j;
    int lengthArray[MAX_H*MAX_V];
    int distance;
    dikstras(hardmap, end, end, true, true, &distance, lengthArray);
    
    for(j = 0; j<MAX_V;j++){
      for(i = 0;i<MAX_H;i++){
	distance = INT_MAX; 
	int index = i+j*MAX_H;
	
	if(index != end){
	  if(hardmap[i][j]!=255){
	    //dikstras(hardmap, index, end, true, true, &distance, lengthArray);
	    distance = lengthArray[index];
	    //printf("%d", distance%10);
	  }
	  if(hardmap[i][j] != 255){
	  Tmap[i][j] = distance;
	  }
	  else{
	  Tmap[i][j] = distance;
	  }
	    
	}
	
	else{
	  //printf(" ");
	  Tmap[i][j] = 0;
	}
	
      }
      //printf("\n");
    }
   //printf("\n");
}

void printNoTunnelMap(player p, int hardmap[MAX_H][MAX_V], int Tmap[MAX_H][MAX_V]){
    
  int i, j;
  for(j = 0; j<MAX_V;j++){
      for(i = 0;i<MAX_H;i++){
	
	
	if((p.x ==i) && (p.y == j)){
	putchar('@');
	  
	}
	else{
	  if(Tmap[i][j]==INT_MAX){
	    if(hardmap[i][j] == 0){ //Unreachable Room
	    printf("\x1b[31;41m");
	    printf("%d", Tmap[i][j] % 10);
	    printf("\x1b[0m");
	    }
	    else{
	    
	      putchar(' ');
	    }
	  
	    
	  }
	  else{

	    printColor(Tmap[i][j] / 10);
	    printf("%d", Tmap[i][j] % 10);
	    printf("\x1b[0m");
	  }
	}
	
	
      }
      printf("\n");
    }
    printf("\n");
  
  
}

void createNoTunnelMap(player p, int hardmap[MAX_H][MAX_V], int Tmap[MAX_H][MAX_V]){

    int end = p.x+p.y*MAX_H;
    int i,j;
    int lengthArray[MAX_V*MAX_H];
    int distance;
    
    
    dikstras(hardmap, end, end, true, false, &distance, lengthArray);
    for(j = 0; j<MAX_V;j++){
      for(i = 0;i<MAX_H;i++){
	distance = INT_MAX;
	int index = i+j*MAX_H;
	if(index != end){
	  if(hardmap[i][j] == 0){
	    
	    Tmap[i][j] = lengthArray[index];
	  }
	  else{
	  Tmap[i][j] = distance;
	  }
	}
	else{
	  //printf(" ");
	  Tmap[i][j] = 0;
	}
	
      }
      //printf("\n");
    }
   //printf("\n");
  
}

void printTunnelMap(player p, int Tmap[MAX_H][MAX_V]){
    
  int i, j;
  for(j = 0; j<MAX_V;j++){
      for(i = 0;i<MAX_H;i++){
	
	
	if((p.x ==i) && (p.y == j)){
	putchar('@');
	  
	}
	else{
	  if(Tmap[i][j]!=INT_MAX){
	  printColor(Tmap[i][j] / 10);
	  printf("%d", Tmap[i][j] % 10);
	  printf("\x1b[0m");
	  }
	  else{
	  putchar(' ');
	    
	  }
	  
	}
	
	
      }
      printf("\n");
    }
    printf("\n");
  
  
}