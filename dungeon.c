#include "dungeon.h"




#ifdef _WIN32
#define ROOMCOLOR ""
#define RESET ""
#else
#define ROOMCOLOR "\x1b[30;43m"
#define RESET "\x1b[0m"
#endif



data endianSwap(data e){
  data final;
  final.str[0] = e.str[3];
  final.str[1] = e.str[2];
  final.str[2] = e.str[1];
  final.str[3] = e.str[0];
  return final;
}

bool checkEndian(){
  int i = 1;
  
  return (*(char *)&i == 1);
  
}

bool checkRoomIntersection(room A, room B){

int Aw = A.w;
int Ah = A.h;
int Bw = B.w;
int Bh = B.h;
int buffer = 2;

//Add one to the bounds, so the bounding box
//guarantees that we have 1 space between rooms
int Aleft = A.x -buffer;
int Atop = A.y-buffer;
int Aright = Aleft+Aw+buffer; 
int Abot = A.y+Ah+buffer;

int Bleft = B.x-buffer;
int Btop = B.y-buffer;
int Bright = Bleft+Bw+buffer; 
int Bbot = B.y+Bh+buffer;

	//Bound Check, if any one of these are true,
	// no overlap can exist.
	if(Aright < Bleft) return false;
	if(Aleft > Bright) return false;
	if(Atop > Bbot) return false;
	if(Abot < Btop) return false;
	
				
	//We're returning True if it overlaps
	return true;
}


int createDungeon(int dungeonmap[MAX_H][MAX_V], int Max_Rooms,room Room[], int rockhardnessmap[MAX_H][MAX_V]){


	
	
	initializeDungeon(dungeonmap,rockhardnessmap,true);
	
	memset(rockhardnessmap,0,MAX_H*MAX_V*sizeof(int));



	int i, j;
	//Initialize the Hardness Map. For now make it random hardness.
	for(j = 0;j<MAX_V;j++){
		for(i=0;i<MAX_H;i++){
		    
		    rockhardnessmap[i][j] = (rand() % 253) +1; 
		}
		
	}
	
	//Make Bounds
	for(i = 0; i<MAX_V; i++){
		rockhardnessmap[0][i] = 255; 
		rockhardnessmap[MAX_H-1][i] = 255; 
	}

	for(i = 0; i<MAX_H; i++){
		rockhardnessmap[i][0] = 255; 
		rockhardnessmap[i][MAX_V-1] = 255; 
	}
	
	
	//Debug Check for Hardness Map
	/*
	for(j = 0;j<MAX_V;j++){
		for(i=0;i<MAX_H;i++){
			if(rockhardnessmap[i][j] < 10){
			putchar(48+rockhardnessmap[i][j]);
			
			}
			else{
				putchar('#');
			}	
		}
		putchar('\n');
	}
	*/


	//Create Room Positions
	//room Room[5];
	//Bound Width is equal to the Max Bounds
	//subtracted by the left and right wall 
	//and a buffer of 2 offsets.
	//The reasoning is for room drawable space.
	int BoundWidth = MAX_H -4;
	int BoundHeight = MAX_V - 4;

	//Set Max Room Width and Height
	int MaxRoomWidth = 7;
	int MaxRoomHeight = 8;
	int MinRoomWidth = 3;
	int MinRoomHeight = 2;

	int offset = 2;
	//Start Random Generation of Rooms
	for(i = 0; i<Max_Rooms;i++){

		int x = rand()%BoundWidth + offset;
		int y = rand()%BoundHeight + offset;
		Room[i].x = x;
		Room[i].y = y;
		Room[i].w = rand()%MaxRoomWidth + MinRoomWidth; 
		Room[i].h = rand()%MaxRoomHeight + MinRoomHeight;
		Room[i].cx = x + Room[i].w/2;
		Room[i].cy = y + Room[i].h/2;
		 
		//Initially say the room isn't valid, to run through loop.
		Room[i].valid = false;

		//Debug Check Top Left, Center, Bottom Right;

		int attempt = 0;
		while(((Room[i].x + Room[i].w)>(MAX_H-2)) || 
			((Room[i].y + Room[i].h)>(MAX_V-2))||
			!Room[i].valid){

			
			int intersectioncount = 0;
			
			x = rand()%BoundWidth + offset;
			y = rand()%BoundHeight + offset;
			Room[i].x = x;
			Room[i].y = y;
			Room[i].w = rand()%MaxRoomWidth + MinRoomWidth; 
			Room[i].h = rand()%MaxRoomHeight + MinRoomHeight;
			Room[i].cx = x + Room[i].w/2;
			Room[i].cy = y + Room[i].h/2;	
			
			int k = 0;
			
			for(k = 0; k<i ; k++){
		
				if(checkRoomIntersection(Room[k],Room[i]) && k!=i){
					Room[i].valid = false;
					intersectioncount++;

					//Debug Check for Intersection and with what.
					//printf("attempt %d : Room Intersection with Room %d and %d and with %d rooms.\n",attempt,k,i,intersectioncount);


					attempt++;
				}
			}
			if(intersectioncount == 0){			
				Room[i].valid = true;
			}

			if(attempt > 4999){
				printf("Not enough space for new room, Max Rooms is now : %d.\n", i-1);
				Max_Rooms = i-1;
				break;		
			}
			
		}
		//Debug Check Room Locations
		/*
		int m;
		int n;
		for(m = Room[i].x; m<(Room[i].x+Room[i].w);m++){
			for(n = Room[i].y; n<(Room[i].y+Room[i].h);n++){
				dungeonmap[m][n] = '.';
			}
		}
		
		
		

		*/
		
	}
	
	

	drawCorridors(dungeonmap,rockhardnessmap,Room,Max_Rooms);
	
	drawCorridorsHardness(dungeonmap, rockhardnessmap);
	
	drawRooms(dungeonmap,rockhardnessmap, Room, Max_Rooms);


	//printDungeon(dungeonmap);




	return Max_Rooms;
}

void drawCorridorsHardness(int dungeonmap[MAX_H][MAX_V], int rockhardnessmap[MAX_H][MAX_V]){
  int i,j;
  for(j = 0; j<MAX_V;j++){
    for(i = 0;i<MAX_H;i++){
    
	if(dungeonmap[i][j] == '#'){
	
	  rockhardnessmap[i][j] = 0;
	}
      
    }
    
  }
  
  
}


void initializeDungeon(int dungeonmap[MAX_H][MAX_V], int rockhardnessmap[MAX_H][MAX_V], bool hardness_set){
	int i;
  	//Make Bounds
	for(i = 0; i<MAX_V; i++){
	  
	  if(rockhardnessmap[0][i] == 255){
	    dungeonmap[0][i] = '|';
	  }
	  
	  if(rockhardnessmap[MAX_H-1][i] ==255){
	    dungeonmap[MAX_H-1][i] = '|';
	  }
	  
	  if(hardness_set){
	    rockhardnessmap[0][i] = 255;
	    rockhardnessmap[MAX_V-1][i] = 255;
	    dungeonmap[0][i] = '|';
	    dungeonmap[MAX_H-1][i] = '|';
	  }
	  
	}

	for(i = 0; i<MAX_H; i++){
	  if(rockhardnessmap[i][0] == 255){
	    dungeonmap[i][0] = '-'; 
	  }
	  if(rockhardnessmap[i][MAX_V-1]==255){
	    dungeonmap[i][MAX_V-1] = '-';
	  }
	  
	  if(hardness_set){
	    rockhardnessmap[i][0] = 255;
	    rockhardnessmap[i][MAX_V-1] = 255;
	    dungeonmap[i][0] = '-';
	    dungeonmap[i][MAX_V-1] = '-';
	  }
	}
}



void printDungeon(int dungeonmap[MAX_H][MAX_V]){
	int i,j;
  	//Print Dungeon Map
	
	for(j = 0; j<MAX_V;j++){ 
		for(i = 0; i<MAX_H;i++){
			if(dungeonmap[i][j] == 0){
				putchar(' ');
			}
			else{
				if(dungeonmap[i][j] == '.')
				printf("\x1b[30;43m");
				putchar(dungeonmap[i][j]);
				printf("\x1b[0m");
			}
		}
		putchar('\n');
	}
	putchar('\n');
}

void drawRooms(int dungeonmap[MAX_H][MAX_V],int hardnessmap[MAX_H][MAX_V], room Room[], int Max_Rooms){
	int i;
  	//Draw Rooms
	for( i = 0; i<Max_Rooms;i++){
		int m;
		int n;
		for(m = Room[i].x; m<(Room[i].x+Room[i].w);m++){
			for(n = Room[i].y; n<(Room[i].y+Room[i].h);n++){
				dungeonmap[m][n] = '.';
				hardnessmap[m][n] = 0;
			}
		}
		
		//Debug Check Room Number
		/*
		switch(i){
			case 0:
				dungeonmap[Room[i].x][Room[i].y] = '0';
			break;
			case 1:
				dungeonmap[Room[i].x][Room[i].y] = '1';
			break;
			case 2:
				dungeonmap[Room[i].x][Room[i].y] = '2';
			break;
			case 3:
				dungeonmap[Room[i].x][Room[i].y] = '3';
			break;
			case 4:
				dungeonmap[Room[i].x][Room[i].y] = '4';
			break;
		}
		*/
	}
  
}

void loadCorridors(int dungeonmap[MAX_H][MAX_V], int rockhardnessmap[MAX_H][MAX_V]){
  int i,j;
  for(j = 0; j<MAX_V; j++){
    for(i = 0; i<MAX_H;i++){
      if(rockhardnessmap[i][j] == 0){
      
	dungeonmap[i][j] = '#';
      }      
    }
  }
  
}

void drawCorridors(int dungeonmap[MAX_H][MAX_V], int rockhardnessmap[MAX_H][MAX_V], room Room[], int Max_Rooms){
	
	int i;
	//Draw Paths To Rooms
	for(i = 0;i<(Max_Rooms-1);i++){
		//Copy the Current Hardness Map.
		int tempHardnessMap[MAX_H][MAX_V];
		memcpy(tempHardnessMap,rockhardnessmap,sizeof(tempHardnessMap));
		
		
		
		int m, n, k;
		
		
		//For All Other Rooms, set hardness map as if it was a wall.
		for( k = 0; k<5;k++){
			if(k!=i && k!=(i+1)){
				for(m = (Room[k].x-1); m<(Room[k].x+Room[k].w+1);m++){
					for(n = (Room[k].y-1); n<(Room[k].y+Room[k].h+1);n++){
						if(m>0 && m<MAX_H && n>0 && n<MAX_V){
							tempHardnessMap[m][n] = 255;
						}
					}
				}
			}
		}
		//Debug Print New Hardness Map
		/*
		for(m = 0;m<MAX_V;m++){
			for(n=0;n<MAX_H;n++){
				if(tempHardnessMap[n][m] < 10){
				putchar(48+tempHardnessMap[n][m]);
				
				}
				else{
					putchar(' ');
				}	
			}
		putchar('\n');
		}
		putchar('\n');
		*/
		//Convert to 1D array for dijkstra's
		int i_start = Room[i].cx + Room[i].cy*MAX_H; 
		int i_end = Room[i+1].cx + Room[i+1].cy*MAX_H;
		//Calculate Shortest Path via Dijkstra's Algorithm.
		
		pList path;
		int length;
		int lengtharray[1680];
		path = dikstras(tempHardnessMap, i_start, i_end, false, false, &length,lengtharray);
		
		for(k = 0;k<path.size;k++){
			int x = path.List[k] % MAX_H;
			int y = path.List[k] / MAX_H;
			dungeonmap[x][y] = '#';
		}
		
	}
	
}

void printHardnessMap(int rockhardnessmap[MAX_H][MAX_V]){

  		//Debug Print New Hardness Map
		int m,n;
		for(m = 0;m<MAX_V;m++){
			for(n=0;n<MAX_H;n++){
				if(rockhardnessmap[n][m] < 255){
				  printColor(rockhardnessmap[n][m] / 85);
				  putchar(48+rockhardnessmap[n][m]%10);
				  printf("\x1b[0m");
				}
				else{
					putchar(' ');
				}	
			}
		putchar('\n');
		}
		putchar('\n');
		
  
}




int loadDungeon(char * filename,int dungeonmap[MAX_H][MAX_V],int rock_hard_map[MAX_H][MAX_V], room Rooms[]){
  int Max_Rooms = -1;
  char* file = malloc(strlen(getenv("HOME"))+strlen("/.rlg327/")+50);
  strcpy(file,getenv("HOME"));
  strcat(file,"/.rlg327/");
  strcat(file,filename);
  FILE* loadfile;
  loadfile = fopen(file, "r");
  
  if(loadfile == NULL){
    printf("File Error : %s\n", strerror(errno));
    return -1;
    
  }
  
  int result = 0;
  int max_file_size;
  
  result++;
  
  
  fseek(loadfile,0,SEEK_END);
  max_file_size = ftell(loadfile);
  rewind(loadfile);
  
  //printf("File Size is %d\n",max_file_size);
  
  
  char * file_data = malloc(max_file_size);
  result = fread(file_data,sizeof(char),max_file_size,loadfile);
  
  //printf("Read %d\n",result);
  char marker[6];
  data version;
  data size;
  
  int pos = 0;
  memcpy(marker,file_data,sizeof(marker)+pos);
  //printf("Header: %s\n",marker);
  pos+=sizeof(marker);
  
  memcpy(version.str,file_data+pos,sizeof(version.str));
  pos+=sizeof(version.str);
  if(checkEndian()){
    version = endianSwap(version);
  }
  //printf("Version: %d\n", version.integer);
  
  memcpy(size.str,file_data+pos,sizeof(size.str));
  pos+=sizeof(size.str);
  if(checkEndian()){
    size = endianSwap(size);
  }
  //printf("file_size: %d\n", size.integer);  
  
  unsigned char map[MAX_V][MAX_H];
  memcpy(map,file_data+pos,sizeof(map));
  
  int i, j;  
  for(j = 0;j<MAX_V;j++){
    for(i = 0; i<MAX_H;i++){
      rock_hard_map[i][j] = (int)map[j][i];
    }
  }
  pos+=(MAX_V*MAX_H);
  
  Max_Rooms = (max_file_size - pos) / 4;
  //printf("There are %d rooms.\n", Max_Rooms);
  
  
  for(i = 0; i<Max_Rooms; i++){
    Rooms[i].y = (int)file_data[pos+i*4];
    Rooms[i].x = (int)file_data[pos+i*4+1];
    Rooms[i].h = (int)file_data[pos+i*4+2];
    Rooms[i].w = (int)file_data[pos+i*4+3];
    Rooms[i].cx = Rooms[i].x + Rooms[i].w/2;
    Rooms[i].cy = Rooms[i].y + Rooms[i].h/2;
      
    //Initially say the room isn't valid, to run through loop.
    Rooms[i].valid = false;
    
    //printf("%d-> Y:%d X:%d H:%d W:%d\n", i, Rooms[i].y, 
	//   Rooms[i].x, Rooms[i].h, Rooms[i].w);
  }
  
  /*
  for(i = 0; i<(Max_Rooms*4); ++i){
  
    printf("%d ",file_data[pos+i]);
  }
  */
  
  
	//Initialize Dungeon Map
	//int dungeonmap[MAX_H][MAX_V];
	//memset(dungeonmap,0,sizeof(dungeonmap));
	
	initializeDungeon(dungeonmap,rock_hard_map,false);
	
	loadCorridors(dungeonmap,rock_hard_map);
	
	drawRooms(dungeonmap,rock_hard_map, Rooms, Max_Rooms);


	//printDungeon(dungeonmap);
	fclose(loadfile);
	free(file);
	free(file_data);
  
return Max_Rooms;
}

int saveDungeon(char * filename,int rock_hard_map[MAX_H][MAX_V], room Rooms[], int Max_Rooms){

  
  unsigned char hardnessmap[MAX_V][MAX_H];
  int i,j;

  for(j=0;j<MAX_V;j++){
    for(i=0;i<MAX_H;i++){
      hardnessmap[j][i] = (unsigned char)rock_hard_map[i][j];
    }
  }
  
  
  char* path = malloc(strlen(getenv("HOME"))+strlen("/.rlg327/")+1);
  char* file = malloc(strlen(getenv("HOME"))+strlen("/.rlg327/")+50);
  strcpy(path,getenv("HOME"));
  strcat(path,"/.rlg327/");

  //printf("%s\n",path);
  
  mkdir(path,0777);
  char mark[7] = "RLG327";
  
  data version;
  version.integer= 0000;
  
  unsigned char hard_map[MAX_H*MAX_V];
  
  memcpy(hard_map,hardnessmap,sizeof(hardnessmap));
  data size;
  size.integer = (int)(sizeof(int)+sizeof(version)+6+sizeof(hardnessmap) + Max_Rooms*4);
  //printf("Total Data to be Written: %d\n", size.integer);
  size = endianSwap(size);
  version = endianSwap(version);
  
  strcpy(file,getenv("HOME"));
  strcat(file,"/.rlg327/");
  strcat(file,filename);
  printf("%s\n",size.str);
  FILE* savefile;
  savefile = fopen(file, "w");
  int result;
  if(savefile == NULL){
    printf("File Error : %s", strerror(errno));
    return Max_Rooms;
    
  }
  
  result = fwrite(mark,sizeof(char),6, savefile);
  //printf("%d, %s\n", result, mark);
  result = fwrite(version.str,sizeof(char),sizeof(version.str), savefile);
  //printf("%d, %d\n", result, version.integer);
  result = fwrite(size.str,sizeof(char),sizeof(size.str),savefile);
  //printf("%d, %d\n", result, size.integer);
  result = fwrite(hardnessmap, sizeof(unsigned char), sizeof(hardnessmap), savefile);
  //printf("%d\n", result);
  result++;

  
  for(i=0;i<Max_Rooms;i++){
    
    data roomdata;
    roomdata.str[0]=(unsigned char)Rooms[i].y;
    roomdata.str[1]=(unsigned char)Rooms[i].x;
    roomdata.str[2]=(unsigned char)Rooms[i].h;
    roomdata.str[3]=(unsigned char)Rooms[i].w;
    
    
    fwrite(roomdata.str, sizeof(char), sizeof(roomdata.str), savefile);
    //printf("%d-> Y:%d X:%d H:%d W:%d\n", i, (int)roomdata.str[0], 
    //(int)roomdata.str[1], (int)roomdata.str[2], (int)roomdata.str[3]);
    
  }
  
  
  
  fclose(savefile);
  
  free(path);
  free(file);
  
return 0;
}



void printColor(int i){

char *KRED  ="\x1B[31m";
char *KGRN  ="\x1B[32m";
char *KYEL  ="\x1B[33m";
char *KBLU  ="\x1B[34m";
char *KMAG  ="\x1B[35m";
char *KCYN  ="\x1B[36m";
  
  switch(i){
    case 0:
      printf("\x1b[0m");
      break;
    case 1:
      printf(KCYN);
      break;
    case 2:
      printf(KMAG);
      break;
    case 3:
      printf(KBLU);
      break;
    case 4:
      printf(KYEL);
      break;
    case 5:
      printf(KGRN);
      break;
    default:
      printf(KRED);
      break;
      
  }
  
  
}


