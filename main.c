#include "dungeon.h"






int main(int argc, char * argv[]){

  
  
  	//Initialize Time for Rand
	time_t ti;

	//Seed the RNG
	srand((unsigned) time(&ti));
  
  int i;
  bool save = false;
  bool load = false;
  bool player_set = false;
  
  int Max_Rooms = 5;
  int rockhardnessmap[MAX_H][MAX_V];
  memset(rockhardnessmap,0,MAX_V*MAX_H*sizeof(int));
  char * filename = malloc(50); //Guarantee Memory Space
  strcpy(filename,"dungeon");
  player p;
  
  for(i = 1;i<argc;i++){
    if(!strcmp(argv[i],"--save")){
      save = true;
    }
    else if(!strcmp(argv[i],"--load")){
      load = true;
    }
    else if(!strcmp(argv[i],"-s")){
      if(i+1<argc){
	Max_Rooms = atoi(argv[i+1]) > 5 ? atoi(argv[i+1]) : 5;
      } 
    }    
    else if(!strcmp(argv[i], "-l")){
      if(i+1<argc){
	//filename = argv[i+1];
	if(filename[0] != '-' && strcmp(argv[i+1],"")){ 
	  //filename = argv[i+1];
	  memset(filename,'\0',50);
	  strcpy(filename,argv[i+1]);
	}
	else{
	  printf("Invalid Load File, using dungeon\n");
	  memset(filename,'\0',50);
	  strcpy(filename,"dungeon");
	}
      }   
    }
    else if(!strcmp(argv[i], "-p")){
      if(i+2<argc){
	p.x = atoi(argv[i+1]);
	p.y = atoi(argv[i+2]);
	player_set = true;
      }   
    }
  }
  
  room Rooms[5000];
  //Initialize Dungeon Map
  int dungeonmap[MAX_H][MAX_V];
  memset(dungeonmap,0,sizeof(dungeonmap));
  
  
  
   
  
if(!load){
  printf("Creating Dungeon\n");
  Max_Rooms = createDungeon(dungeonmap,Max_Rooms,Rooms,rockhardnessmap);
}
else{

  printf("Loading Dungeon\n");
  int testMax_Rooms = loadDungeon(filename, dungeonmap, rockhardnessmap,Rooms);
  if(testMax_Rooms == -1){
    testMax_Rooms = Max_Rooms;
    printf("Creating Dungeon\n");
    Max_Rooms = createDungeon(dungeonmap,testMax_Rooms,Rooms,rockhardnessmap);
  }
  else{
  
    Max_Rooms = testMax_Rooms;
  }
  
}

if(save){
  printf("Saving Dungeon\n");  
  saveDungeon(filename, rockhardnessmap,Rooms, Max_Rooms);  
  
}






  int cRoom = rand() % Max_Rooms;
  int x = Rooms[cRoom].x;
  int y = Rooms[cRoom].y;
  int w = Rooms[cRoom].w;
  int h = Rooms[cRoom].h;
  bool inRoom = false;
  
  if(!player_set){
    p.x = x + rand() % w;
    p.y = y + rand() % h;
  }
  else{
	if(rockhardnessmap[p.x][p.y] == 0){
	printf("located in room\n");
	inRoom = true;
	}
  }
  
  
  
  if(!inRoom){
    if(player_set){
    printf("-p %d %d arg is not in a room.\n", p.x, p.y);
    player_set = false;
    }
  }
  
  if(!player_set){
    p.x = x + rand() % w;
    p.y = y + rand() % h;
  }
  
  
  
  printf("%d, %d\n",p.x,p.y);
  int TunnelMap[MAX_H][MAX_V];
  int NoneTunnelMap[MAX_H][MAX_V];

  dungeonmap[p.x][p.y] = '@';
  printDungeon(dungeonmap);
  
  
  createNoTunnelMap(p, rockhardnessmap,NoneTunnelMap);
  printNoTunnelMap(p,rockhardnessmap, NoneTunnelMap);
  
  createTunnelMap(p, rockhardnessmap,TunnelMap);
  printTunnelMap(p,TunnelMap);

  free(filename);
return 0;
}