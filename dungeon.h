#ifndef DUNGEON_H
#define DUNGEON_H


typedef struct playerdata{
  int x;
  int y;
} player;

#include "dijkstras.h"

#include <errno.h>
#include <sys/stat.h>

typedef union _data{
  char str[4];
  int integer;
} data;


typedef struct roomdata{
	//X and Y are position of top left of the room.
	int x; 
	int y;
	int w;
	int h;
	int cx;
	int cy;
	bool valid;
} room;





bool checkRoomIntersection(room A, room B);
int createDungeon(int dungeonmap[MAX_H][MAX_V], int Max_Rooms, room Room[], int rockhardnessmap[MAX_H][MAX_V]);
int saveDungeon(char * filename, int rock_hard_map[MAX_H][MAX_V], room Rooms[], int Max_Rooms);
int loadDungeon(char * filename, int dungeonmap[MAX_H][MAX_V], int rock_hard_map[MAX_H][MAX_V], room Rooms[]);
data endianSwap(data e);
void initializeDungeon(int dungeonmap[MAX_H][MAX_V], int rockhardnessmap[MAX_H][MAX_V], bool hardness_set);
void printDungeon(int dungeonmap[MAX_H][MAX_V]);
void drawRooms(int dungeonmap[MAX_H][MAX_V],int hardnessmap[MAX_H][MAX_V], room Rooms[], int Max_Rooms);
void drawCorridors(int dungeonmap[MAX_H][MAX_V], int rockhardnessmap[MAX_H][MAX_V], room Room[], int Max_Rooms);
void drawCorridorsHardness(int dungeonmap[MAX_H][MAX_V], int rockhardnessmap[MAX_H][MAX_V]);
void printHardnessMap(int rockhardnessmap[MAX_H][MAX_V]);
void loadCorridors(int dungeonmap[MAX_H][MAX_V], int rockhardnessmap[MAX_H][MAX_V]);

//Monster.c 
void createTunnelMap(player p, int hardmap[MAX_H][MAX_V],int Tmap[MAX_H][MAX_V]);
void createNoTunnelMap(player p, int hardmap[MAX_H][MAX_V],int Tmap[MAX_H][MAX_V]);

void printNoTunnelMap(player p,int hardmap[MAX_H][MAX_V], int Tmap[MAX_H][MAX_V]);
void printTunnelMap(player p, int Tmap[MAX_H][MAX_V]);
void printColor(int i);

#endif