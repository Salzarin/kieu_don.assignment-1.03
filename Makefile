all:	Dungeon
	


Dungeon: main.c dungeon.c dijkstras.c adjacencylist.c monster.c
	gcc -g -Wall -o Dungeon main.c dungeon.c monster.c dijkstras.c adjacencylist.c
clean:
	rm -f Dungeon *o *~
tar:
	tar cvfz ./../kieu_don.assignment-1.02.tar.gz ./../kieu_don.assignment-1.02/ --exclude .git
changelog:
	git log --date=local --pretty=format:"%ad, %h, %an, message: %s" > CHANGELOG
